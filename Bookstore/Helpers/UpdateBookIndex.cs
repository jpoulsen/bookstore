﻿using Bookstore.Models;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace Bookstore.Helpers
{
    public static class UpdateBookIndex
    {
        private static string SearchFolderName = "CachedBookResult";
        private static object _lock;

        static UpdateBookIndex()
        {
            _lock = new object();
        }

        public static void AddToBookIndex(IEnumerable<BookViewModel> BookData)
        {
            if (Monitor.TryEnter(_lock))
            {
                try
                {
                    string UpdateFolder = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "App_Data", "Indexes", SearchFolderName);
                    StandardAnalyzer analyzer = new StandardAnalyzer(global::Lucene.Net.Util.Version.LUCENE_30);
                    var directory = FSDirectory.Open(new DirectoryInfo(UpdateFolder));
                    using(IndexWriter writer = new IndexWriter(directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED))
                    {
                        foreach (var book in BookData)
                        {
                            // remove older index entry
                            var searchQuery = new TermQuery(new Term("Isbn13", book.Isbn13));
                            writer.DeleteDocuments(searchQuery);

                            // add entry to index
                            writer.AddDocument(addToDocument(book));
                        }
                        writer.Optimize();
                        writer.Commit();
                    }

                }
                catch (Exception ex)
                {
                } 
                finally
                {
                    Monitor.Exit(_lock);
                }
            }
        }

        public static void UpdateSingleBookIndex(BookViewModel book)
        {
            if (Monitor.TryEnter(_lock))
            {
                try
                {
                    string UpdateFolder = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "App_Data", "Indexes", SearchFolderName);
                    StandardAnalyzer analyzer = new StandardAnalyzer(global::Lucene.Net.Util.Version.LUCENE_30);
                    var directory = FSDirectory.Open(new DirectoryInfo(UpdateFolder));
                    IndexWriter writer = new IndexWriter(directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED);

                    // remove older index entry
                    var searchQuery = new TermQuery(new Term("Isbn13", book.Isbn13));
                    writer.DeleteDocuments(searchQuery);

                    // add entry to index
                    writer.AddDocument(addToDocument(book));
                    
                    writer.Optimize();
                    writer.Commit();
                    writer.Dispose();

                }
                catch (Exception ex)
                {
                }
                finally
                {
                    Monitor.Exit(_lock);
                }
            }
        }

        private static Document addToDocument(BookViewModel book)
        {
            //writes new entry

            var doc = new Document();

            doc.Add(new Field("Isbn10", book.Isbn10 ?? string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("Title", book.Title ?? string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("Author", book.Author ?? string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("Id", book.Id ?? string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("Labels", book.Label ?? string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("ImageUrl", book.ThumbnailUrl ?? string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("Price", book.Price ?? string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("NormalPrice", book.NormalPrice ?? string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("Url", book.Url ?? string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field("Isbn13", book.Isbn13 ?? string.Empty, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new NumericField("Checked", Field.Store.YES, true).SetIntValue(book.Checked));

            return doc;
        }

    }
}