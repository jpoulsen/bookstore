﻿using Bookstore.Helpers;
using Bookstore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Bookstore.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> SuggestBooks(string IsbnTextArea)
        {
            if(!string.IsNullOrEmpty(IsbnTextArea))
            {
                string[] sep = new string[] { "\r\n" };
                string[] isbnSearchIds = IsbnTextArea.Split(sep, StringSplitOptions.RemoveEmptyEntries);

                var finalResult = new List<BookViewModel>();

                using (var repo = new Repository())
                {
                    finalResult = await repo.getBookData(isbnSearchIds);
                }

                return PartialView("_BookPartialView", finalResult);
            }
            return PartialView("_BookPartialView");

        }

        [HttpPost]
        public void UpdateBook(string Isbn13, int isChecked)
        {
            if(!string.IsNullOrEmpty(Isbn13) && Isbn13.Length == 13)
            using(var repo = new Repository())
            {
                repo.SaveBookUpdate(Isbn13, isChecked);
            }

        }

    }
}