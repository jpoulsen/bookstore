﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore.Models
{
    public class PriceProduct
    {
        public class Rootobject
        {
            public Price[] prices { get; set; }
        }

        public class Price
        {
            public string id { get; set; }
            public int quantity { get; set; }
            public string normalprice { get; set; }
            public string price { get; set; }
            public string vatpercent { get; set; }
            public string fixedprice { get; set; }
            public string discount { get; set; }
            public int allowdiscount { get; set; }
            public string productid { get; set; }
            public Entry[] entries { get; set; }
        }

        public class Entry
        {
            public string id { get; set; }
            public string type { get; set; }
            public int quantity { get; set; }
            public string vatpercent { get; set; }
            public string vatamount { get; set; }
            public string priceunitexvat { get; set; }
            public string priceunitinclvat { get; set; }
            public string pricetotalexvat { get; set; }
            public string pricetotalinclvat { get; set; }
            public string discountpercent { get; set; }
            public string discountamountexvat { get; set; }
            public string discountamountinclvat { get; set; }
            public string DiscountEntityType { get; set; }
        }

    }
}
