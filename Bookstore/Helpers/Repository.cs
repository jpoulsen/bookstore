﻿using Bookstore.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace Bookstore.Helpers
{
    public class Repository : DataObject
    {

        string ApiKey = ConfigurationManager.AppSettings["ApiKey"];
        string ApiUrl = ConfigurationManager.AppSettings["ApiUrl"];

        public async Task<List<BookViewModel>> getBookData(string[] IsbnIds)
        {
            var finalResult = new List<BookViewModel>();
            var finalIndexResult = new List<BookViewModel>();


            foreach (var Isbn in IsbnIds)
            {
                if (!string.IsNullOrEmpty(Isbn) && Isbn.Length == 13)
                {
                    var bookDataModel = new Book();

                    var result = new BookViewModel();
                    result = SearchBookIndex.FindBook(Isbn);

                    //book exist in index
                    if (result != null)
                    {
                        finalResult.Add(result);

                    }
                    else
                    {
                        var dbResult = Db.Books.FirstOrDefault(b => b.Isbn13 == Isbn);

                        //book exist in Db
                        if (dbResult != null)
                        {
                            result = MapBook(dbResult);
                            finalResult.Add(result);
                        }
                        //book does not exist in index or db get from api
                        else
                        {
                            var fullApiDescUrl = string.Empty;
                            var fullApiPriceUrl = string.Empty;

                            fullApiDescUrl = string.Format(ApiUrl + "/products/products.json?key={0}&isbn={1}", ApiKey, Isbn);

                            using (var client = new HttpClient())
                            {
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                // HTTP GET
                                HttpResponseMessage responseProduct = await client.GetAsync(fullApiDescUrl);
                                if (responseProduct.IsSuccessStatusCode)
                                {
                                    var resultProductJson = await responseProduct.Content.ReadAsAsync<Products.Rootobject>();

                                    if (resultProductJson != null)
                                    {
                                        var product = resultProductJson.products.FirstOrDefault();

                                        //map to db model
                                        bookDataModel.BookId = Int32.Parse(product.id);
                                        bookDataModel.ImageUrl = product.imageurl;
                                        bookDataModel.Url = product.url;
                                        bookDataModel.Author = product.contributors.FirstOrDefault().name;
                                        bookDataModel.Isbn10 = product.isbn10;
                                        bookDataModel.Isbn13 = product.isbn13;
                                        bookDataModel.Labels = product.label;
                                        bookDataModel.Title = product.title;
                                        bookDataModel.Checked = false;
                                    }
                                }

                                fullApiPriceUrl = string.Format(ApiUrl + "/Prices/Prices.json?key={0}&ProductId={1}", ApiKey, bookDataModel.BookId);
                                HttpResponseMessage responsePrice = await client.GetAsync(fullApiPriceUrl);
                                if (responsePrice.IsSuccessStatusCode)
                                {
                                    //get Price
                                    var resultPriceJson = await responsePrice.Content.ReadAsAsync<PriceProduct.Rootobject>();
                                    if (resultPriceJson != null)
                                    {
                                        var price = resultPriceJson.prices.FirstOrDefault();
                                        bookDataModel.Price = price.price;
                                        bookDataModel.NormalPrice = price.normalprice;
                                    }

                                }

                                try
                                {
                                    Db.Books.Add(bookDataModel);
                                    Db.SaveChanges();
                                }
                                catch (Exception e)
                                {

                                }
                            }

                            result = MapBook(bookDataModel);
                            finalIndexResult.Add(result);
                        }
                    }
                }
            }

            if (finalIndexResult.Any())
            {
                //save to index for faster loading
                UpdateBookIndex.AddToBookIndex(finalIndexResult);
                finalResult.AddRange(finalIndexResult);
            }

            return finalResult;
        }

        public void SaveBookUpdate(string Isbn13, int isChecked)
        {
            var book = Db.Books.FirstOrDefault(b => b.Isbn13 == Isbn13);

            if(book != null)
            {
                book.Checked = isChecked == 1 ? true : false;

                var books = new List<BookViewModel>();
                books.Add(MapBook(book));
                UpdateBookIndex.AddToBookIndex(books);

                Db.SaveChanges();
            }


        }


        public BookViewModel MapBook(Book book)
        {
            return new BookViewModel()
            {
                Author = book.Author,
                Checked = book.Checked ? 1 : 0,
                Isbn10 = book.Isbn10,
                Isbn13 = book.Isbn13,
                Id = book.BookId.ToString(),
                Label = book.Labels,
                Price = book.Price,
                NormalPrice = book.NormalPrice,
                Url = book.Url,
                ThumbnailUrl = book.ImageUrl,
                Title = book.Title
            };
        } 
    }
}