﻿using Bookstore.Models;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace Bookstore.Helpers
{
    public static class SearchBookIndex
    {
        private static FSDirectory directory;
        private static StandardAnalyzer analyzer;

        private static string SearchFolderName = "CachedBookResult";
        private static object _lock;

        static SearchBookIndex()
        {
            _lock = new object();

        }

        public static BookViewModel FindBook(string Isbn)
        {
            if (Monitor.TryEnter(_lock))
            {
                try
                {
                    string SearchFolder = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "App_Data", "Indexes", SearchFolderName);
                    analyzer = new StandardAnalyzer(global::Lucene.Net.Util.Version.LUCENE_30);
                    using(var directory = FSDirectory.Open(new DirectoryInfo(SearchFolder)))
                    {
                        var finalResult = new BookViewModel();

                        var pq = new PhraseQuery();
                        pq.Add(new Term("Isbn13", Isbn));

                        var searcher = new IndexSearcher(directory, true);

                        // final query
                        TopDocs topDocs = searcher.Search(pq, 1);

                        //   int results = topDocs.ScoreDocs.Length;
                        if (topDocs.TotalHits != 0)
                        {
                            ScoreDoc scoreDoc = topDocs.ScoreDocs[0];
                            int docId = scoreDoc.Doc;
                            Document doc = searcher.Doc(docId);

                            return mapBookModel(doc);
                        }
                    }

                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    Monitor.Exit(_lock);

                }
            }

            return null;
        }

        private static BookViewModel mapBookModel(Document doc)
        {
            var result = new BookViewModel()
            {
                Author = doc.Get("Author"),
                Title = doc.Get("Title"),
                Isbn10 = doc.Get("Isbn10"),
                ThumbnailUrl = doc.Get("ImageUrl"),
                Url = doc.Get("Url"),
                Price = doc.Get("Price"),
                NormalPrice = doc.Get("NormalPrice"),
                Label = doc.Get("Labels"),
                Id = doc.Get("Id"),
                Isbn13 = doc.Get("Isbn13"),
                Checked = Int32.Parse(doc.Get("Checked"))
            };

            return result;
        }
    }
}