﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bookstore.Models
{
    public class BookViewModel
    {
        public string Id { get; set; }

        public string Isbn10 { get; set; }

        public string Isbn13 { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string ThumbnailUrl { get; set; }

        public string Url { get; set; }

        public string Price { get; set; }

        public string NormalPrice { get; set; }

        public string Label { get; set; }

        public int Checked { get; set; }
    }

}