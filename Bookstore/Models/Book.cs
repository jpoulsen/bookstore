namespace Bookstore.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Book")]
    public partial class Book
    {
        public int Id { get; set; }

        public int BookId { get; set; }

        [StringLength(10)]
        public string Isbn10 { get; set; }

        [StringLength(13)]
        public string Isbn13 { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public string ImageUrl { get; set; }

        [StringLength(255)]
        public string Labels { get; set; }

        [StringLength(50)]
        public string Price { get; set; }

        [StringLength(50)]
        public string NormalPrice { get; set; }

        public bool Checked { get; set; }
    }
}
