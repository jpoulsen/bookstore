﻿using Bookstore.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Bookstore.Helpers
{
    public class DataObject : IDisposable
    {
        public DataModel Db { get; private set; }

        public DataObject()
        {
            Db = new DataModel();
        }

        ~DataObject()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Dispose managed resources
                if (Db != null)
                {
                    Db.Dispose();
                    Db = null;
                }
            }

            // Dispose unmanaged resources
        }
    }
}